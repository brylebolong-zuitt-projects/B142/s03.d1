package com.bolong.b142.s02.s02app.services;

import com.bolong.b142.s02.s02app.models.Post;
import com.bolong.b142.s02.s02app.models.User;

public interface PostService {

    void createPost(Post newPost);
    void updatePost(Long id, Post updatedPost);
    void deletePost(Long id);
    Iterable<Post> getPosts();
    Iterable<Post> getMyPosts(Long userId);

    Object getPosts(Long userId);
}
