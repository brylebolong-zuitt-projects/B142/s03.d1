package com.bolong.b142.s02.s02app.services;

import com.bolong.b142.s02.s02app.models.Post;
import com.bolong.b142.s02.s02app.models.User;
import com.bolong.b142.s02.s02app.repositories.PostRepository;
import com.bolong.b142.s02.s02app.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImplementation  implements PostService{
    @Autowired
    private PostRepository postRepository;
    private UserRepository userRepository;

    public void createPost(Post newPost) {
        postRepository.save(newPost);
    }

    public void updatePost(Long id, Post updatedPost){
        Post existingPost = postRepository.findById(id).get();
        existingPost.setTitle(updatedPost.getTitle());
        existingPost.setContent(updatedPost.getContent());
        postRepository.save(existingPost);
    }

    public void deletePost(Long id){
        postRepository.deleteById(id);
    }

    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }

    public Iterable<Post> getMyPosts(Long userId) {
        User author = userRepository.findById(userId).get();
        return author.getPosts();
    }
    @Override
    public Object getPosts(Long userId) {
        return null;
    }
}
