package com.bolong.b142.s02.s02app.services;

import com.bolong.b142.s02.s02app.models.User;

public interface UserService {
    void createUser(User newUser);
    void updateUser(Long id, User updatedUser);
    void deleteUser(Long id);
    Iterable<User> getUser();
}
