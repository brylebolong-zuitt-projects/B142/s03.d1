package com.bolong.b142.s02.s02app.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;


@Entity
@Table(name="users")
public class User {

    // Properties  (columns)
    @Id
    @GeneratedValue
    private Long id; // primary key
    @Column
    private String UserName;
    @Column
    private String Password;
    @OneToMany(mappedBy = "User")
    @JsonIgnore
    private Set<Post> posts;
    // Constructors
    public User() {}

    public User(String UserName, String Password){
        this.UserName = UserName;
        this.Password = Password;
    }

    // Getters & Setters

    public String getUserName() {
        return UserName;
    }
    public void setUserName(String UserName) { this.UserName = UserName;}

    public String getPassword() {
        return Password;
    }
    public void setPassword(String Password) { this.Password = Password;}

    public Set<Post> getPosts(){
        return posts;
    }

}
